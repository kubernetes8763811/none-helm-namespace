<details>
<summary> Click to reveal note</summary>

- The **values.yaml file and template directory** 
reflect what the private helm chart template looks like.

- The **Create Helm Chart for Java App** section shows how the helm chart was created

- **poc** directory holds proof concept. Has no direct relation to this project.
</details>



******

<details>
<summary> Create a Kubernetes cluster </summary>
 <br />

**steps:**

```sh
# Login to my Linode account
# Create a Cluster in any Region Shared CPU (cost effective) select an Image, choose the amount of Nodes and provision it
# Download kubeconfig.yaml file
# Set configuration file to use when executing kubectl commands by setting the location as an env variable.
# Set permission of the file using chmod 400 fileName. ReadOnly accessfor the current user
export KUBECONFIG=/location/kubeconfig.yaml
chmod 400 /location/kubeconfig.yaml

```

</details>

******

<details>
<summary> Deploy Mysql with 2 replicas </summary>
 <br />

**steps**

```sh

#Create a namespace to organise my work. See nonehelmns.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: none-helm-namespace

# Set none-helm-namespace as the default namespace
kubectl config set-context --current --namespace=none-helm-namespace

# Check if default namespace is updated
kubectl config view --minify --output 'jsonpath={..namespace}'


# Create custom values file for mysql helm chart. See helm-mysql.yaml file
architecture: replication
primary:
  persistence:
    storageClass: "linode-block-storage"
secondary:
  replicaCount: 1
  persistence:
    storageClass: "linode-block-storage"
auth:
  rootPassword: "rootpass"
  createDatabase: true
  database: "*********"
  username: "*********"
  password: "*********"

# Add bitnami repo
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install mysql --values helm-mysql.yaml bitnami/mysql

helm repo ls


```

</details>

******

<details>
<summary> Deploy your Java Application with 2 replicas </summary>
 <br />

**steps**

```sh
# Build the application executing the command gradle clean; gradle build
# Create a Dockerfile for the java application
# Application can be found here https://gitlab.com/kubernetes8763811/kubernetesjavaapp
# Ensure you are signed in to dockerHub in commandline. Run command docker login
# Build the image using buildx because it is will run on an amd64 architechture. Image name and tag
docker buildx build --platform linux/amd64 -t kachidude/demo-app:k8v2-1.2 .
docker push kachidude/demo-app:k8v2-1.2


# Create a Secret by providing credentials in commandline. 
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>

kubectl get secret

# Create config file and execute command to create pods for my java application. See myjava.yaml file for complete code 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myjavaservice
spec:
  replicas: 2
  selector:
    matchLabels:
      app: myjavaservice

# Create Secret for mysql-secret (secretKeyRef) and mysql-configmap (ConfigMap)
# See mysql-secret.yaml file
# See mysql-configmap.yaml
# Create both (secret and configmap)components in cluster

kubectl apply -f mysql-secret.yaml
kubectl apply -f mysql-configmap.yaml
kubectl apply -f my-java-app.yaml

```

</details>

******

<details>
<summary> Deploy phpmyadmin </summary>
 <br />

**steps**

```sh
# Create config file and execute command to create pod for phpmyadmin application. See my-sql-admin.yaml file for complete code
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysqladminservice
spec:
  replicas: 1
  Line 20   value: mysql-secondary # Execute kubectl get all to see mysql service name. Secondary replica for ReadOnly permission

kubectl apply -f my-sql-admin.yaml

```

</details>

******

<details>
<summary> Deploy Ingress Controller Using Helm</summary>
 <br />

**steps**

```sh
# Add the helm repo that contains ingress nginx
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true # set gives ingress an external ip
kubectl get ingress -n none-helm-namespace

```

</details>

******

<details>
<summary> Create Ingress rule </summary>
 <br />

**steps**

```sh
# Create ingress rule, check helm-ingress.yaml for complete configuration
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: my-java-app-ingress


```

</details>

******

<details>
<summary> Port-forward for phpmyadmin </summary>
 <br />

**steps**

```sh
kubectl port-forward service/mysqladminservice 8084:8084

```

</details>

******

<details>
<summary> Create Helm Chart for Java App </summary>
 <br />

**steps**

```sh
# For seperation of concerns create a new directory for the chat
# mkdir helm-chart-template //Updated to k8s-helm-chart-template-10-ex-8 directory locally
# cd helm-chart-template //k8s-helm-chart-template-10-ex-8

helm create myappjava

# Create template for all components (Deployment, Secret, ConfigMap, regcred and service) see template folder
# Create set variables in values.yaml file, using camelCase as variable format 
# In values.yaml file, replace base64-encoded literal with the actual base64 encoded value.

# From the helm-chart-template directory 
helm template -f values.yaml myappjava # To see the outcome of executing the install (Validation)

# If you have a custom file, you can replace the values.yaml file with your custom file

helm install -f /Users/johnikeson/Developments/devops-bootcamp/kubernetes-intro/helm-chart-template/myappjava/values.yaml myjavaapp myappjava # myjavaapp (name you intent to refer to the helm chart in your cluster) and myappjava (actual chart name)

helm repo ls






```

</details>

******

<details>
<summary>Exercise 9: Configure automatic triggering of multi-branch pipeline </summary>
 <br />

**steps**

```sh
# Add branch based logic to Jenkinsfile


# Add webhook to trigger pipeline automatically
# Add a build strategy using Ignore Committer Strategy plugin
# End



```

</details>
